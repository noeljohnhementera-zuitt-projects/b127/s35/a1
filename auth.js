// Authentication = verification
// Authorization = giving access

// Token is hindi nakikita ng user

// Dito isasave ang json web token
	// It helps us to build a token everytime na may mag-lologin
		// Kapag maglog-in lang ang user ginagamit ang json web token

// JSON web token or JWT is a way of securely passing information from the server to the 
	// frontend or to other part of server
// Information is kept secure through the use of the secret code
	// only the system knows the secret code that can decode the encrypted information
	// secret is equal to a lock code

const jwt = require("jsonwebtoken");
// to create a payload in order to pass data sa users

// User defined string data that will be used to create our JSON web tokens
// used in the algorithm for encrypting our data which makes it difficult to decode the
	// information w/out the defined secret keyword

const secret = "CourseBookingAPI";
// Encryption in our ticket
// para secure ang info
	// server lang ang may alam ng secret
// pwede palitan ng kahit anong string value


// Token Creation

// Analogy = pack the gift and provide a lock with the secret code as the key
module.exports.createAccessToken = (user) =>{
	// The data will be received from the registration form
		// so when the user logs in, a token will be created with users information
	const data = {																	// This is the payload
		// data na manggagaling sa registered user
		id: user._id, // property from stored user
		email: user.email,
		isAdmin: user.isAdmin
	};
	// We used the parameter user to user the properties na ginawa sa model (User.js)

	// Generate a JSON Web Token using the jwt's sign method(signature)
	// Generates the token using the form data and the secret code with no addition options provided
	return jwt.sign(data, secret, {})
}
// If tama ang email at parehas ang password, gagana ang code
// END
// START
// s33 DISCUSSION

 // Token Authorization / Verification

 // Analogy = receive the gift and open the lock to verify if the sender is legitimate and the gift
 	// was not tampered with

 	module.exports.verify = (req, res, next) =>{		// dahil may client- server interaction na kaya req, res
 		// The token is retrieved from the request header
 		// This can be provided in Postman under 
 			// Authorization > bearer token
 		let token = req.headers.authorization; 			// Unlock palang ang gift or iverify. Hindi pa bubuksan
 															// Next step palang
 														// Kinuha lang si token
 														// ibibigay once na authenticated si user (Bearer token sa Postman)
 		// Token received and is not undefined
 		if(typeof token !== "undefined"){
 			console.log(token);

 		// The token that we receive is just like this
 			// "Bearer ibmlfiksdhjmflihvmif" Automatic na lumalabas ang bearer
 		// The .slice method takes only the token from the information sent via the request header
 		// This removes the "bearer" prefix and obtains only the token for verification
 		token = token.slice(7, token.length);

 		// Validate the token using the "verify" method decrypting the token using the secret code
 		return jwt.verify(token, secret, (err, data) => {
 			// if JWT is not valid
 			if(err){
 				return res.send( {auth: "failed"} )
 			}else{
 			// id JWT is valid
 				next() // if maverify na maayos, magproceed na next function or statement (.decode)
 			}
 			// Allows the application to proceed with the next middleware function/callback function in the route
 			// The next() middleware will be used to process to another function that invokes the contorller function (business logic)
 			// next() is a callback function sa routes

 		})

 		}else{
 			// Token does not exist
 			return res.send( {auth: "failed"} );
 		}
 	}


 	// Token Decryption

 	// Analogy = Open the gift and get the content
 	module.exports.decode = (token) =>{			/// Kinukuha na natin ang data

 		// Token received and is not defined
 		if(typeof token !== "undefined"){ // If hindi undefined ang token
 			// Retrieves only the token and removes the "bearer" prefix
 		token = token.slice(7, token.length);

 		return jwt.verify(token, secret, (err, data) =>{

 			if(err){
 				return null;
 			}else{
 				// The "decode" method is used to obtain the info from the JWT
 				// {complete : true} option allows us to return additional info from the JWT token
 					// it returns an object with an access to the "payload" which contains user information
 						// stored when the token was generated
 				// The payload contains the info provided in the "createAAccessToken" method defined above (id, email, and password)
 				return jwt.decode(token, {complete: true}).payload		// this acts as the data parameter. yung igenerate na data
 			}															// data ang lalabas sa Postman (authorization > Bearer token)
 		})
 		// Token does not exist
 	}else{
 		return null;
 	}

 }