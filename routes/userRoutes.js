
// Activity at Line 33

const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
// We need to require express in order to use the router

const auth = require("../auth");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res)=>{
	userControllers.checkEmailExists(req.body)
	.then(result => {
		res.send(result);
	});
})

// Route for user registration
router.post("/register", (req, res)=>{
	userControllers.registerUser(req.body)
	.then(result => {
		res.send(result)
	})
})

// Route for User Authentication
router.post("/login", (req, res)=>{
	userControllers.loginUser(req.body)
	.then(result => {
		res.send(result)
	})
})

// Start of s33 Activity
router.get("/allUsers", (req, res) =>{
	userControllers.allUsers()
	.then(users =>{
		res.send(users)
	})
})

router.post("/details", (req, res)=>{
	userControllers.getProfile(req.body)		// kukunin yung Body sa Postman gamit ang getProfile na controller
	// userControllers.getProfile(req.body.id)  // this is also viable. specific data
	// userControllers.getProfile( {userId: req.body.id} )
	.then(userId => {
		res.send(userId)
	})
})

// End of s33 Activity


// Between userRoutes.js at auth.js lang ang invole dito sa decoding

// The auth.verify acts as a middleware to ensure that the user is logged in before they can access their profile

router.get("/details", auth.verify, (req, res)=>{	 // if authorized after verification, saka pala iauthorize (.then)
	// Uses the "decode" method defined in the suth.js file to retrieve the user information from the
		// token passing the "token" from the request bearer as an srgument
	const userData = auth.decode(req.headers.authorization)	// authorization sa postman
	userControllers.getProfile({userId: userData.id})		// userData is the variable that we set
	.then(result =>{
		res.send(result)
	})
})

module.exports = router;