const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

// Route for creating a course
// Base url, localhost:4000/courses/
router.post("/", auth.verify, (req, res) =>{ 		// we need auth verify if included sya sa data variable sa auth.js
	
	// Check if user is admin or not
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin	// we use auth.decode dahil kasama sa token (auth.js) ang isAdmin
	}
	console.log(data)					// We added this one para lumabas sa terminal to determine if isAdmin = true

	courseControllers.addCourse(data)	// data yung variable
	.then(result =>{					
		res.send(result);
	})
})


// Retrieve all courses
router.get("/all", (req, res)=>{
	courseControllers.getAllCourses()
	.then(result =>{
		res.send(result);
	})
})

// Retrieve all true courses
router.get("/", (req, res)=>{
	courseControllers.getAllActive()
	.then(result =>{
		res.send(result);
	})
})

// Retrieve specific course
router.get("/:id", (req,res)=>{
	courseControllers.specificCourse(req.params.id)
	.then(result =>{
		res.send(result)
	})
})

// Update a course
router.put("/:id", auth.verify, (req, res)=>{

	const data = {					// kinukuha ang isAdmin
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
		// course: req.body,
		// id: req.params

	courseControllers.updateCourse(data, req.body, req.params.id)
	.then(result=>{
		res.send(result)
	})
})

module.exports = router;