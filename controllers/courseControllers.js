const Course = require("../models/Course");

/*
Creation of Course
Business Logic
1. Create a condtional statement that will check if the user is an admin
2. Create a course object using the mongoose model and the information from the request body and the id
	// from the header
3. Save the new course to the database
*/

module.exports.addCourse = (data) =>{
	// if user is an admin
	if(data.isAdmin){							// tanggalin muna ang validation (if, else) dahil may server error
		let newCourse = new Course({
			name: data.course.name,					// data is the parameter
			description:data.course.description,	// course is found in data variable under controllers
			price: data.course.price
		})
		return newCourse.save()
		.then((course, error) =>{
			// course creation faile
			if(error){
				return false;
			}else{
				return true
			}
		})

	}else{
		return false;
	}
}

// Retrieve all courses
module.exports.getAllCourses = () =>{
	return Course.find({})
	.then(result =>{
		return result;
	})
}

// Retrieve all true courses
module.exports.getAllActive = () =>{
	return Course.find( {isActive: true} )
}

// Retrieve specific course
module.exports.specificCourse = (courseId) =>{
	return Course.findById(courseId)
}


// Update a course
/*
1. Check if the user is an admin
2. Create a variable which will contain the information retrieved from the req.body
3. Find and update the course using the courseId retrieved from the req.params property and the
	// variable containing the information form the request bdy
*/

// SUCCESSFUL Logic
// Update a course

module.exports.updateCourse = (data, courseNew, courseId) =>{
	return Course.findById(courseId)
	.then((res, err)=>{
		if(err){
			return false
		}else{
			if(data.isAdmin){
				res.name = courseNew.name
				res.description = courseNew.description
				res.price = courseNew.price
				return res.save()
				.then((result, error)=>{
					if(error){
						return false
					}else{
						return result
					}
				})
			}
		}
	})
}


/*// FAILED TRIALS

// Yesterday Trials 
	// Output is always auth = "failed"

// Trial 1 - Update a course
module.exports.updateCourse = (courseId, newCourse, data) =>{
	if(data.Admin){
		return Course.findById(courseId)
		.then((result, err)=>{
			if(err){
				return false;
			}else{
				let newCourse = new Course({
					name: data.name,
					description: data.description
				}) 
				return newCourse.save()
			}
		})
	}
}


// Trial 2 - Update a course
module.exports.updateCourse = (courseId, newCourse, data) =>{
	return Course.findById(courseId)
	.then((result, error)=>{
		if(error){
			return false;
		}
		result.name = newCourse.name;
		return result.save()
		.then((updCourse, err)=>{
			if(err){
				return false;
			}else{
				return true;
			}

		})
	})
}	

// Trial 3 - Update a course
module.exports.updateCourse = (courseId, newCourse, data) =>{
	return Course.findById(courseId)
	.then((result, error)=>{
		if(data.isAdmin){
			result.name = newCourse.name;
			return (result)
		}else{
			return false;
		}
	})
}*/